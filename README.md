# docs

Documentation for RISIS developpers

## License

- Copyright (c) 2019 Philippe Breucker
- Copyright (c) 2019 Cristian Martinez
- Copyright (c) 2020-2023 Université Gustave Eiffel
- Copyright (c) 2020-2023 INRAE

Licensed under the EUPL.

The full text of the EUPL licence can be found at
https://opensource.org/licenses/EUPL-1.2.
