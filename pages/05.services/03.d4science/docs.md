---
title: D4Science
class: linkless
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;"> D4Science is a Hybrid Data Infrastructure serving a number of Virtual Research Environments exploited in the context of several European projects and international partnerships. D4Science will design, deploy, and operate a VRE (Virtual Research Environment) to foster interaction between RISIS-RCF and OpenAire with the view to have the RISIS project to contribute to the OSaaS (Open Science as a Service), while linking a Community Based Infrastructure in science policy and innovation studies. </p>