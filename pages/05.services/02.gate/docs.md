---
title: Gate Cloud
class: linkless
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;"> GATE Cloud is complementary to CorTexT, as it provides numerous open-source multilingual natural language processing tools and services for text analysis, on a document-by-document basis. This includes lexical analysis, Named Entity Recognition (NER), social media analysis, entity linking with Linked Open Data (LOD) resources), and ontology-based semantic annotation via the KNOWMAK annotation service (see INDICATORS TOPICS-ONTOLOGY page). More services will be made available in the RCF as the project progresses.</p>

![gate-cloud-service.png](gate-cloud-service.png?cropResize=800)