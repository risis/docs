---
title: CorTexT
class: linkless
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;"> CORTEXT is a platform for on-line processing of heterogeneous textual corpuses and currently it operates as a platform offering researchers different ways to enrich and analyse their corpuses. CorTexT provides text mining tools and socio-semantics analysis for corpus-level (inter-document) analysis, including data parser (txt, doc, json, ris and other proprietary format such as WoS, Scopus, Factiva,…), term extraction, generation of word2vec models, interaction networks (e.g. retweet networks), clustering, geocoding, and graph-based community detection.  It also give user access to numerous data viz such as sankey “tubes”, network map, geographic map, etc.</p>

