---
title: OpenAIRE
class: linkless
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">  It is the European Data Infrastructure for Scientific Open Access. CNR-ISTI is responsible of the OpenAIRE development and pre-production infrastructure. Specific and relevant databases and contents will be imported from the OpenAIRE infrastructure, through the implementation of a VRE by D4Science. </p>