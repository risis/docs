---
title: Registration
taxonomy:
  category: docs

---
![register.gif](register.gif?1200&classes=shadow)
- Go to the RCF Workspace home page: [https://rcf.risis.io](https://rcf.risis.io).
- You will be redirected to the Risis Central Authentication Page.
- Click on the ‘Register’ link, at the bottom of the login form.
![registration0.png](registration0.png?cropResize=800)
- Fill in all the fields (First name, last name, email, password, confirm password).
![registration1.png](registration1.png?cropResize=800)
- Click on the ‘Register’ button
- The system will register you and will redirect you to the home page. 
