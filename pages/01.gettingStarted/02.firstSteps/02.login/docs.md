---
taxonomy:
  category: docs

---
## Log in to RCF Workspace 

**Note:** *if you just registered, you will then be logged in automatically, you don’t have to go through these steps. You still need to log in to the Datastore though, see below.*

![login.gif](login.gif?resize=1200&classes=shadow)

- Go to the **RCF Workspace** home page: [https://rcf.risis.io](https://rcf.risis.io).
- Enter your email and password.
![login-rcf.png](login-rcf.png?cropResize=800)
- Click on the ‘Sign In’ button.
- The system will log you in, and will redirect you to the home page. 

## Log in to RCF Datastore

**Note:** *As for the RCF beta version, log in the RCF Datastore as well as Workspace is required, with the same credentials, to smooth the navigation between Workspace and Datastore. This step should disappear in the future. This procedure describes how to do it semi-automatically, in a few clicks.*

*In the case, you’re not logged into RCF Workspace beforehand (for example if you come directly to the Datastore page), after step 3 you will need to enter your credential as described in the workspace log in above, and you will then be redirected to the datastore home page.*

- Log in to the RCF Workspace first (see above).
- Go to the  RCF Datastore login page: [https://datastore.risis.io/loginpage.xhtml?redirectPage=%2Fdataverse_homepage.xhtml](https://datastore.risis.io/loginpage.xhtml?redirectPage=%2Fdataverse_homepage.xhtml).
- Click on the ‘RCF - IAM’ button located in the ‘Other options’ section.
![login-datastore.png](login-datastore.png?cropResize=800)
- Click on the ‘Log in with RCF - IAM’ button.
![login-datastore-1.png](login-datastore-1.png?cropResize=800)
- You may have to confirm some personal data. After confirmation, you will be logged in to the RCF Datastore, and your session should be kept alive coming from the workspace.