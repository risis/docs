---
title: Search In Workspace
taxonomy:
  category: docs

---



![search-in-workspace.gif](search-in-workspace.gif?resize=1200&classes=shadow)

- Access your account and navigate to the homepage.

- Select the "Search in Workspace" option.

- Enter the desired search term.




