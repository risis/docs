---
title: Explore Results
taxonomy:
  category: docs
---
![view-result.gif](view-result.gif?resize=1200&classes=shadow)

- In the top menu, go to ‘Projects’ and select ‘Explore’.
![view-results0.png](view-results0.png?cropResize=800)
- Select the project where you run the scenario in our case ‘Patents extraction’
- There are two options to go to the outputs page:
    * In the ‘Explorer’ section, click on the outputs button located at the top of the project card.
    ![view-results1.png](view-results1.png?cropResize=300)
    * In the ‘Explorer’ section, click on the title to go to the project profile and click in the ‘Outputs’ sub-menu located over the cards.
    ![view-results2.png](view-results2.png?cropResize=600)
- After that, the system will open a new page with a list of the results. 
    * In the left part, you will find all executions done sorted by execution date.
    ![view-results3.png](view-results3.png?cropResize=800)
    * When you select one, at the right you will see the list of the result.
    ![view-results4.png](view-results4.png?cropResize=800)
- In this case, there is only one result, could be more, depending on the scenario. Each row has some action when you pass the mouse over it.
      * **Eye:** To open the result in a new web tab
      ![view-results5.png](view-results5.png?cropResize=800)
      * **Delete:** To delete the output if you don’t need it.
      ![view-results6.png](view-results6.png?cropResize=860)
- If you want, you can download the results in a .zip file. There are two possibilities to do it.
      * **Big button:** Select one of the results in the left table, and after at the top of the page, you will see a ‘Download outputs’ button. Please click on it.
      ![view-results7.png](view-results7.png?cropResize=700)
      * **Small button:** In the history table, located in the left part, you will see a small download green icon, please click on the icon to download the outputs.
      ![view-results8.png](view-results8.png?cropResize=300)