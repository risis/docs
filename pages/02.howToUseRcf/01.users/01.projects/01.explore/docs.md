---
title :  Explore Projects
taxonomy:
  category: docs
---
![explore-project.gif](explore-project.gif?resize=1200&classes=shadow)
1. Go to the RCF workspace homepage [https://rcf.risis.io](https://rcf.risis.io). 
2. In the top menu, go to ‘Projects’ and select ‘Explorer’.
![explore-project0.png](explore-project0.png?cropResize=800)
3. The system will show all your projects. Click on the title of the project that you want to access.
![explore-project1.png](explore-project1.png?cropResize=800)
4. You can search for some specific projects by writing in the search bar. For this example, we selected 'test'. You can search in title, status, author, description or in all these fields. It can be ordered by date, name, author or status. You can also choose the order to be ascending or descending.
![explore-project2.png](explore-project2.png?cropResize=800)