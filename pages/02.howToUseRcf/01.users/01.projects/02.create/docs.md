---
title :  Create Project
taxonomy:
  category: docs
---
![create-project.gif](create-project.gif?resize=1200&classes=shadow)
1. Go to the RCF workspace homepage [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Create’.
![create-project0.png](create-project0.png?cropResize=800)
3. The system will show a small form. Please, fill in the title and description of the project.
![create-project1.png](create-project1.png?cropResize=800)
4. Click on the ‘Create’ button.
5. The system will create a project in the database and will redirect you to the project view.
![create-project2.png](create-project2.png?cropResize=800)