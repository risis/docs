---
title :  Remove Project
taxonomy:
  category: docs
---
![delete-project.gif](delete-project.gif?resize=1200&classes=shadow)
1. Go to the RCF home page [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Explorer’.
![delete-project0.png](delete-project0.png?cropResize=800)
3. The system will show all your projects. Click on the title of the project that you want to access. In this example, it will be ‘Eupro Project’.
![delete-project1.png](delete-project1.png?cropResize=800)
4. On the left part, you will see the “Delete” button.
![delete-project2.png](delete-project2.png?cropResize=800)
5. A modal will appear to confirm your action. Please hit ‘Yes’ to delete the project
![delete-project3.png](delete-project3.png?cropResize=800)