---
title :  Edit Project
taxonomy:
  category: docs
---
![edit-project.gif](edit-project.gif?resize=1200&classes=shadow)
1. Go to the RCF workspace homepage [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Explorer’.
![edit-project0.png](edit-project0.png?cropResize=800)
3. The system will show all your projects, click on the title of the project that you want to access. In this example it will be ‘Eupro Project’.
![edit-project1.png](edit-project1.png?cropResize=800)
4. On the left part, you will see the title and description of the project. If you want, you can edit it. 
![edit-project2.png](edit-project2.png?cropResize=800)
5. After editing the title, you will see the next successful message.
![edit-project3.png](edit-project3.png?cropResize=800)
6. After editing the description, you will see this message.
![edit-project4.png](edit-project4.png?cropResize=800)