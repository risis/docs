---
title: Import Scenario
taxonomy:
  category: docs
---

To be able to run a scenario, you have to import it into a project first. Before that, you should create a project (see procedure above). There are two ways to import a scenario: either from the existing project board page or via the “Scenario -> Explore” page.

### From the project view page
![import-scenario0.gif](import-scenario0.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Explore’.
![import-scenario0.png](import-scenario0.png?cropResize=800)
3. The system will show all your projects, click on the title of the project that you want. In this example, it will be ‘Eupro Project’.
![import-scenario1.png](import-scenario1.png?cropResize=500)
4. Now, in the left part of the project view, you will see the title, description, and some buttons to save, delete, or archive. At the same time, you will see in the right section a grid of the datasets and scenarios already imported (in this case, none so far). To import a new scenario, click on the section ‘Import scenarios‘.
![import-scenario2.png](import-scenario2.png?cropResize=600)
5. You can search a specific scenario. Then, select one of them.
![import-scenario3.png](import-scenario3.png?cropResize=400)
6. Click on the ‘+’ button at the right.
![import-scenario4.png](import-scenario4.png?cropResize=400)
7. The scenario is imported successfully into the project.
![import-scenario5.png](import-scenario5.png?cropResize=800)

### From the list of scenarios on page
![import-scenario1.gif](import-scenario1.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Scenarios’ and select ‘Explore’.
![import-scenario6.png](import-scenario6.png?cropResize=600)
3. The system will show a list of scenarios. Search the scenario that you want to import and click on the ‘import’ button located at the top of each card. 
![import-scenario7.png](import-scenario7.png?cropResize=800)
4. A modal will open showing the title of the scenario and an input field to search and select the project where the scenario will be imported. After selecting, click on the import button.
![import-scenario8.png](import-scenario8.png?cropResize=700)
5. The system will redirect you to the project profile with the new scenario imported successfully.
![import-scenario9.png](import-scenario9.png?cropResize=800)