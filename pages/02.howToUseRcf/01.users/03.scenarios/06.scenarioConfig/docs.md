---
title: Scenario Configuration
taxonomy:
  category: docs

---


![Scenario-config.gif](scenario-config.gif?resize=1200&classes=shadow)

- In the outputs history section, there is a compilation of execution records. By clicking on the button "Configurations", it allows you to load the parameters used in a specific execution. This would facilitate the configuration of a new scenario based on those parameters.


