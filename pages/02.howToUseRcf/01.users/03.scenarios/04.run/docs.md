---
title: Run Scenario
taxonomy:
  category: docs
---
![run-scenario.gif](run-scenario.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. Create a project. For this example, we created a project named “Patents extraction”.
3. Import the scenario ‘Patents Extraction for Cortext’.
![run-scenario0.png](run-scenario0.png?cropResize=800)
4. Create and import a dataset where the results of the execution will be stored. For this case, we created a dataset named “Dataset for Patents Extraction”.
![run-scenario1.png](run-scenario1.png?cropResize=800)
5. Click in ‘Configure’
![run-scenario2.png](run-scenario2.png?cropResize=300)
6. You will see three principal sections on the page.
   * **Search section:** If you want, you can write a word and the system will apply a filter selecting inputs fields that contains in the title the word inputted.
   ![run-scenario3.png](run-scenario3.png?cropResize=400)
   * **List of the inputs:** This is a list with all inputs that you can configure to run a scenario. Some of them are required.
   ![run-scenario4.png](run-scenario4.png?cropResize=400)
   * **Configurable section:** In this part, each input value has an input field to write a new value, you can change as you want with correct values. Remember to fill **ALL** values required to run the scenario.
   ![run-scenario5.png](run-scenario5.png?cropResize=500)
7. Each input has some actions to do if you want, located at the right of each input.
    * **Reset Option:** If the input has a default value, the system will change the value to the default value. 
    ![run-scenario6.png](run-scenario6.png?cropResize=800)
    * **Clear Option:** The system will clear the input field
    ![run-scenario7.png](run-scenario7.png?cropResize=800)
    * **Change Option:** In some cases, there are some values by default, and you can change the value by clicking on this option.
    ![run-scenario8.png](run-scenario8.png?cropResize=800)
    * **Info Option:** If you need some information, please click on this option
    ![run-scenario9.png](run-scenario9.png?cropResize=800)
8. For this scenario, we need to fill two fields.
    * **Application id:** We will put a number 58, and after pressing the key ‘Enter’ on your keyboard or click on the add button.
    ![run-scenario10.png](run-scenario10.png?cropResize=800)
    The idea is that the number stays like this:
    ![run-scenario11.png](run-scenario11.png?cropResize=800)
    * **Dataset:** We need to select one of the datasets. For our case, we will select the dataset imported previously (‘Dataset for Patents Extraction’).
    ![run-scenario12.png](run-scenario12.png?cropResize=800)

9. When all the required fields will stay filled. The ‘Run this scenario’ button will be available to run. 
![run-scenario13.png](run-scenario13.png?cropResize=800)

10. In order to facilitate the categorization of each scenario configuration execution, users require a method for labeling them. This designated label can subsequently be displayed within the executions list. This can be accomplished by incorporating a straightforward text field within the configuration view, where a placeholder, such as the execution number or date-time, can be pre-set for user convenience.
![run-scenario14.png](run-scenario14.png?cropResize=800)

Within this list of executions, you will easily spot the execution labeled as "patents-extraction-58," as previously mentioned.
![run-scenario15.png](run-scenario15.png?cropResize=800)

After clicking on 'Run this scenario", you will see a green notification saying that the scenario is running.
![run-scenario16.png](run-scenario16.png?cropResize=800)
11. After some time (Depending on the scenario and the tasks), you will receive a notification with the result of the operation (A green notification means that the scenario runs successfully, but if you receive a red notification, it means that there was an error during the execution). You can see the notification in the bell located next to your profile image in the top menu.
    * **Notification of execution failed**
    ![run-scenario17.png](run-scenario17.png?cropResize=800)
    * **Notification of an execution successfully**
    ![run-scenario18.png](run-scenario18.png?cropResize=800)
12. The execution of a scenario was done successfully.