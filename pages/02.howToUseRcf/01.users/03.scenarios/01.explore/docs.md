---
title :  Explore Scenarios
taxonomy:
  category: docs
---
![explore-scenario.gif](explore-scenario.gif?resize=1200&classes=shadow)
1. Go to the RCF home page [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Scenarios’ and select ‘Explore’.
![explore-scenario0.png](explore-scenario0.png?cropResize=800)
3.   The system will show a list of scenarios available for you.
![explore-scenario1.png](explore-scenario1.png?cropResize=800)
4. You can search for some specific scenarios by writing in the search bar. For this example, we selected 'eupro'. You can search in name, inputs, tags, author, description or in all these fields. It can be ordered by date, title, inputs or tags. You can also choose the order to be ascending or descending.
![explore-scenario2.png](explore-scenario2.png?cropResize=800)