---
title: Stop Scenario
taxonomy:
  category: docs

---


![stop-scenario.gif](stop-scenario.gif?resize=1200&classes=shadow)

- Once you've chosen your project and the specific scenario you intend to run, you have the flexibility to halt it whenever you want.

