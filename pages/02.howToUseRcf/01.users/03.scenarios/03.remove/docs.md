---
title :  Remove Scenario
taxonomy:
  category: docs
---
![unlink-scenario.gif](unlink-scenario.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Explore’.
![unlink-scenario0.png](unlink-scenario0.png?cropResize=800)
3. The system will show all your projects. Click on the title of the project that you
want to access. In this example it will be ‘Eupro Project’.
![unlink-scenario1.png](unlink-scenario1.png?cropResize=800)
4. The system will show you all your datasets and scenarios imported into the project. Search the scenario you want to unlink. Click on the three dots located on the top of the card and click on the “Remove” option.
![unlink-scenario2.png](unlink-scenario2.png?cropResize=800)
5. Now the scenario should be removed.