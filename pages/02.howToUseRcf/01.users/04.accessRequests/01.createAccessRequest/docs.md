---
title: Create an Access Request
taxonomy:
  category: docs
---


### Steps

1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. Click 'Access Request' item in navbar, then 'Create an Access Request' 
  ![create-access-request-navbar.png](create-access-request-navbar.png?cropResize=800)

3. After going to the Access Request form page, select at least one dataset
  ![select-datasets.png](select-datasets.png?cropResize=800)
4. After selecting the datasets, proceed to fill the form
  ![create-access-request-form.png](create-access-request-form.png?cropResize=800)
  **Please note that you need to:** 
   - Fill all the fields
   - Select at least one dataset to be main dataset
      - If the main datasets is distant, the secondary datasets need to also be distant access type
      - If physical access type is selected, additional form inputs will appear and they need to be filled.
   - Upload your CV in a PDF format
5. Click the 'Submit' button at the bottom of the form to submit the access request
6. After access request is successfully submited you will be redirected to the details page of the access request
  ![access-request-details.png](access-request-details.png?cropResize=800)
The details page consists of the following elements:
    - **Details**:
        - **Evaluation progress**: Shows all the steps of the evaluation and their status. Steps that are evaluated positively are marked in green color, the current step in the evaluation progress is marked in orange color, if the access request is in revision, the second step will be marked in yellow.
        - **Access Request details**: Shows the access request details filled by the user
        - **Datasets**: Shows the information about the datasets that are selected for the access request
    - **Evaluation**: Shows evaluation details for every evaluation step. If an evaluation step is pending, it will not be shown in the details.
     ![access-request-evaluation.png](access-request-evaluation.png?cropResize=800)

7. You can view your access requests by clicking on 'My Access Requests'
![my-access-requests.png](my-access-requests.png?cropResize=800)

8. Then, you will be redirected to this page (see bellow), where you will find a list of your access requests.
![my-access-requests-list.png](my-access-requests-list.png?cropResize=800)