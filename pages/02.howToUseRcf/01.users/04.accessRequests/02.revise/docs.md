---
title: Access request revision
taxonomy:
  category: docs
---

### Access Request Revision by the requestor/applicant

 If the main access manager of the access request has submited an evaluation that the access request needs revision then the access request will be pending for the user to revise it. The user will be notified by email that they need to revise their access request. 
 When they go to the access request details page, they will see the following  ![access-request-details-revise.png](access-request-details-revise.png?cropResize=800)
 Seeing the yellow step in the evaluation progress is what indicates that this access request needs to be revised by the requestor/applicant.
 <p>
 In order to go to the revision page, the user needs to click the higlighted link in red from the screenshot above.
 </p>
 The link sends the user to the revision page, where will be a pre-filled form with the access request details.
 ![revise-form.png](revise-form.png?cropResize=800)

 Here, the requestor can edit any of the access request fields and then submit the changes after which the Access Manager will evaluate/review the revised access request.  

 ![revise-confirm.png](revise-confirm.png?cropResize=800)

 Optinally, if the applicant wishes to cancel the access request, they can do so by clicking the red '<b>Cancel Access Request</b>' button at the top right of the form.

 ![cancelled-access-request.png](cancelled-access-request.png?cropResize=800)
