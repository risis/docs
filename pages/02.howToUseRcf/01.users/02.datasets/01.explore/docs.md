---
title :  Explore Datasets
taxonomy:
  category: docs
---
![explore-dataset.gif](explore-dataset.gif?resize=1200&classes=shadow)
1. Go to the RCF home page [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Datasets’ and select ‘Explore’.
![explore-dataset0.png](explore-dataset0.png?cropResize=800)
3.  The system will show a list of datasets.
![explore-dataset1.png](explore-dataset1.png?cropResize=800)
4. You can search for some specific datasets by writing in the search bar. For this example, we selected 'cib'. You can search in title, format, keywords, category, author, description or in all these fields. It can be ordered by date, title or author. You can also choose the order to be ascending or descending.
![explore-dataset2.png](explore-dataset2.png?cropResize=800)