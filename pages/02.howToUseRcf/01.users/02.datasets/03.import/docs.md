---
title: "Import Dataset"
taxonomy:
  category: docs
---
Before you can run a scenario that requires access to a dataset, you must import it into an existing project. First, if you did not already, create a project (see procedure above).

### From the project view page
![import-dataset0.gif](import-dataset0.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Explore’.
![import-dataset0.png](import-dataset0.png?cropResize=800)
3. The system will show all your projects, click on the title of the project that you want. In this example, it will be ‘Eupro Project’.
![import-dataset1.png](import-dataset1.png?cropResize=500)
4. Now, in the left part of the project view, you will see the title, description, and some buttons to save, delete, or archive. At the same time, you will see in the right section a grid of the datasets and scenarios already imported (in this case, none so far). To import a new dataset, click on the section ‘Import dataset’.
![import-dataset2.png](import-dataset2.png?cropResize=600)
5. You can search a specific dataset. Then, select one of them.
![import-dataset3.png](import-dataset3.png?cropResize=400)
6. Click on the ‘+’ button at the right.
![import-dataset4.png](import-dataset4.png?cropResize=400)
7. The dataset is imported successfully into the project.
![import-dataset5.png](import-dataset5.png?cropResize=800)

### From the list of datasets on page
![import-dataset1.gif](import-dataset1.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Datasets’ and select ‘Explore’.
![import-dataset6.png](import-dataset6.png?cropResize=600)
3. The system will show a list of datasets. Search the dataset that you want to import and click on the ‘import’ button located at the top of each card. 
![import-dataset7.png](import-dataset7.png?cropResize=800)
4. A modal will open showing the title of the dataset and an input field to search and select the project where the dataset will be imported. After selecting, click on the import button.
![import-dataset8.png](import-dataset8.png?cropResize=700)
5. The system will redirect you to the project profile with the new dataset imported successfully.
![import-dataset9.png](import-dataset9.png?cropResize=800)
