---
title :  Remove Dataset
taxonomy:
  category: docs
---
![unlink-dataset.gif](unlink-dataset.gif?resize=1200&classes=shadow)
1. Login into [https://rcf.risis.io](https://rcf.risis.io).
2. In the top menu, go to ‘Projects’ and select ‘Explore’.
![unlink-dataset0.png](unlink-dataset0.png?cropResize=800)
3. The system will show all your projects, click on the title of the project that you
want to access. In this example, it will be ‘Eupro Project’.
![unlink-dataset1.png](unlink-dataset1.png?cropResize=800)
4. The system will show you all your datasets and scenarios imported into the project. Search the dataset you want to unlink. Click on the three dots located on the top of the card and click on the “Remove” option.
![unlink-dataset2.png](unlink-dataset2.png?cropResize=800)
5. Now the dataset should be removed.