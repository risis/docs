---
title: "Upload Dataset"
taxonomy:
  category: docs
---

RCF allows you to upload your own datasets directly in your workspace, to be used later as input in scenarios. The uploaded dataset files will be uploaded in the RCF datastore, and in your workspace. Only you can access it unless you give access to other users.


![upload-dataset0.gif](upload-dataset0.gif?resize=1200&classes=shadow)
1. Go to the RCF home page [https://rcf.risis.io](https://rcf.risis.io).
2. Drop your files here or click on the image to select them.
![upload-dataset0.png](upload-dataset0.png?cropResize=800)
3. When a file is selected, the system will set the name of the dataset as the name of one of the files you uploaded, but you can change it.
![upload-dataset1.png](upload-dataset1.png?cropResize=800)
4. Click on ‘Start upload’.
5. The system will create a dataset with the file you uploaded and redirect you to the datasets list.


