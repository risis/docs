---
title: Dataset Extraction
taxonomy:
  category: docs
---

There are several scenarios for data extraction in RCF. However, the scenarios bellow are not visible to a normal user. He needs first to create an access request in order to access to one of the Rcf datasets. 
- CIB Firms Extraction
- Grant Dataset Permission
- Linking OrgReg or CIB data with RPD
- Patents Extraction for Cortext
- VICO Extraction
- NANO Documents Extraction
- RPD Extraction

## Query Factory

<p style="text-align: justify;">
The Query Factory is a user-friendly tool for creating custom search queries. Among other things, It allows to easily navigate multiple (api-enabled) datasets, specify search criteria on fields, access a library of predefined queries and validate input data.</p>

![query-factory1.png](query-factory1.png?cropResize=800)

<p style="text-align: justify;">
The Query Factory allows to create and execute searches in RISIS (api-enabled) datasets in a reliable and reproducible way. The Datasets are now organized in a  3-level structure: Source, Entity and Field. When the data comes from a single source, a 2-level menu is built instead.</p>

![query-factory.gif](query-factory.gif?resize=1200&classes=shadow)

<p style="text-align: justify;">
Independently of the elaborated data schemes, a user friendly interface help the user to search and include fields, chose target criteria and validate input data.</p>

![query-factory1.gif](query-factory1.gif?resize=1200&classes=shadow)

<p style="text-align: justify;">
The Query Factory  enables the construction of new datasets that combine different heterogeneous sources to produce MetaSchema-based subdatasets. The Query Factory is not a relational database query language, but a tool aimed  to assist the creation and sharing of search queries. The model behinds its based in the idea of a 3-level description to query a dataset : source (origin), entity (category) and field (piece of data).</p>

![query-factory2.gif](query-factory2.gif?resize=1200&classes=shadow)



Ultimately, you can save your template for the Query Factory, overwrite or delete it in case you've previously saved one.

1. Generate your custom query.
2. Click the "Save Template" button.
3. Enter the title and description; saving should proceed without any issues.
4. Modify your query, then select "Save Template." You can choose to overwrite the existing template or create a new one.
5. Delete your template when needed.
![query-factory3.gif](query-factory3.gif?resize=1200&classes=shadow)

<p style="text-align: justify;">
Last, but not least, Query Factories are fully described in RSML. This allows to include search criterias not only for the RISIS datasets, but also for any kind of task processing that can be described in RSML.</p>