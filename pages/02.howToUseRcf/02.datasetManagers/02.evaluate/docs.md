---
title: Access Request Evaluation 
taxonomy:
  category: docs
---

### Access Request evaluation by access managers

In order to see the pending evaluations, an access managers needs to visit <a href="https://rcf.risis.io/access-request/list/evaluations">https://rcf.risis.io/access-request/list/evaluations</a>, or through the platform click **Access Requests** then **Evaluations**
  ![evaluations-list-navbar.png](evaluations-list-navbar.png?cropResize=800)

They will be redirected to the evaluations list, where they will see all the access requests that are pending evaluation. 
![evaluations-list.png](evaluations-list.png?cropResize=800)
When the list is loaded, by default are shown only the access requests that are pending evaluation by you, that can be changed by selecting different filters based on what access requests you want to see.
<p>To go to the evaluation page, click on the <b>Evaluate</b> button of an access request, if you want to go to the details page, click the <b>Details</b> button. </p>

The evaluation page for the main access manager looks like the following:
![evaluation-form.png](evaluation-form.png?cropResize=800)
The top of the page contains access request details, the bottom part is the form. For the main access managers there is three options for the decision. Selecting **Negative** will result in access request cancellation, and the user will be notified by email. Selecting **Revise**, will result in the access request going into revision, for which the user will be notified by email, after which they need to log in and submit revision of the access request in order for the evaluation to continue.

<p>The secondary access manager form looks like the following: </p>

![sam-evaluation-form.png](sam-evaluation-form.png?cropResize=800)
<p>Selecting <b>Negative</b> in decision will not result in access request cancellation, only this specific dataset from the access request will not be accepted in the end. </p>

<p><span style="color: red">Important for all evaluators</span>: Please write a detailed reasoning when you evaluate negatively or for revision in order for the applicant to be best informed on how to proceed further. </p>
