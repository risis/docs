---
title: CIB / CinnoB 
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>CIB / CinnoB</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">CinnoB (former CIB) provides data and indicators at the firm level for the worldwide largest R&amp;D corporate performers (about 4 000 parent companies and their 350 000 consolidated subsidiaries) that represent over 90% of world corporate industrial R&amp;D. It includes a database of indicators related to R&amp;D and innovation corporate activities at the firm level and, and a register to track back the links between the different sources used to produce the data. The indicators will originate from different resources available in the RISIS consortium: IFRIS-PASTAT, the CWTS Publication Database, EUPRO, the Industrial R&amp;D Scoreboard and Orbis (aggregated data at the parent parent company level). This large set of resources will allow producing data and indicators related to: the corporate knowledge production (Patents/Trademarks/Publications/FP projects), R&amp;D resources in firms and the general activities in firms (financial, sectoral data). Data and indicators will be produced for a period of time of 10 years (2005 -2015) (before and after the crisis). For patents the data will cover the period of time from 1995 to 2015 to provide data for panel analysis. In this sense, CinnoB is used to study many aspects of the dynamics of scientific and technological knowledge creation in large worldwide companies. Services currently offered by the infrastructure: CinnoB is designed as a robust and user-friendly repository of indicators providing standardised indicators at the firm level (including the parent company and consolidated subsidiaries activities). It will cover many dimensions, i.e. industrial sector, technologies, scientific domains, geography, collaborations or financial information. It will benefit from specific recent developments such as the geocoding of addresses, the identification of key actors, and the tagging of thematic priorities. CinnoB will provide a diverse set of quantitative information to study the dynamics of knowledge production at fine scale in large firms in a given sector, technology or geographical space.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>corporation; invention; patent</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://core.ac.uk/download/pdf/48330918.pdf">https://core.ac.uk/download/pdf/48330918.pdf</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3338122/files/Documentation_CIB_Final_RISIS1_version.pdf?download=1" target="_blank">https://zenodo.org/record/3338122/files/Documentation_CIB_Final_RISIS1_version.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Université Paris-Est Marne-la-Vallée (UPEM)
                <a title="view webpage" href="http://www.u-pem.fr/">http://www.u-pem.fr/</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Patricia Laurens
                <a title="email to Access Manager" href="mailto: patricia.laurens@esiee.fr"> patricia.laurens@esiee.fr </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>