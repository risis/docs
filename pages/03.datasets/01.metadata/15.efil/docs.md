---
title: EFIL 
taxonomy:
  category: docs
---

<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>EFIL</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td>
              <p style="text-align: justify;">
              EFIL - European dataset of public R&amp;D funding instruments aims at enabling users to investigate public R&amp;D funding in Europe at the level of project funding instruments and Research Funding Organizations (RFO), addressing questions related to policy design and policy implementation. Objectives of EFIL are: (i) re-composing and characterizing the portfolios of funding instruments of relevant RFOs from selected European countries (AT; CH; CZ; DE; DK; EE; FR; IT; NO; UK); (ii) producing evidence of the structural and allocational aspects of funding instruments. The dataset provides a set of descriptors with an emphasis on a general characterization of the instrument (aim, mode of funding, composition of the decision-making body, etc.); funding allocation criteria and eligible beneficiaries; and funding amounts allocated. Following a first wave of data collection that assumed 2017 and 2018 as reference years for the re-composition of portfolios (release 1.0), a second wave has been completed, updating data up to 2021 (release 2.0). Budgetary data are covered for the period 2021-backwards to 2010. A peculiar feature of the facility is the possibility to characterize the instruments through key words and vocabularies. Indeed, the facility is complemented by a repository of official documents hosted on a cloud - composed of instrument calls, guidelines for participants, descriptions on official webpages, etc. - that is accessible to the database user. EFIL is integrated with other RISIS registers and datasets: OrgREG through the ID of the RFOs managing the funding instruments; and with NATPRO (a module of the EUPRO database) and SIPER, using the ID of the funding instruments.
              </p>
            </td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>R&amp;D Policy Instruments, Project Funding, Research Funding Organizations</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://www.frontiersin.org/articles/10.3389/frma.2021.712839/full">https://www.frontiersin.org/articles/10.3389/frma.2021.712839/full</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/6367802#.YjSN5TUo-Mq" target="_blank">https://zenodo.org/record/6367802#.YjSN5TUo-Mq</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                IRCrES-CNR
                <a title="view webpage" href="http://www.ircres.cnr.it">http://www.ircres.cnr.it</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Andrea Orazio Spinello
                <a title="email to Access Manager" href="mailto: andrea.spinello@ircres.cnr.it"> andrea.spinello@ircres.cnr.it </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>

