---
title: VICO 
taxonomy:
  category: docs
---

<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>VICO</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">VICO contains geographical, industry and accounting information on start-ups that received at least one venture capital investment in the period 1998-2021. Start-ups have been incorporated from 1988 onwards in the 27 countries of the European Union, plus United Kingdom and Israel. VICO also provides information on venture capital investment deals (e.g. deal date, total amount invested) and investors (e.g. investor type, investor experience, and its geographical location). Its uniqueness lies in the overall number of companies (53,714), the country coverage, and the extent of information gathered, thanks to the combination of multiple secondary data sources, namely Thompson One Private Equity, Zephyr, Crunchbase and Orbis. Services currently offered by the infrastructure: VICO allows addressing a wide range of research questions concerning the characteristics, the evolution and the role of venture capital in Europe. It has been widely used by a large academic community to study the peculiarities of the European venture capital market and effectiveness of (different forms of) venture capital in supporting the performance of European start-ups. Results from academic research based on VICO data also attracted the attention of policy makers interested in assessing the impact of policy measures aimed at facilitating access to finance to start-ups. During the RISIS projects, several scholars had the opportunity to access the data and to interact with researchers of POLIMI, which provided advice and customized support.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>start-ups; venture capital; VC; entrepreneurial ventures</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://www.sciencedirect.com/science/article/pii/S0048733323000252">https://www.sciencedirect.com/science/article/pii/S0048733323000252</a>  <br>
                              <a title="view use case" href="https://link.springer.com/article/10.1007/s40812-018-0098-9">https://link.springer.com/article/10.1007/s40812-018-0098-9</a>  <br>
                              <a title="view use case" href="https://link.springer.com/article/10.1007/s40812-019-00113-1">https://link.springer.com/article/10.1007/s40812-019-00113-1</a>  <br>
                              <a title="view use case" href="https://www.sciencedirect.com/science/article/pii/S0929119920302170">https://www.sciencedirect.com/science/article/pii/S0929119920302170</a>  <br>
                              <a title="view use case" href="https://www.sciencedirect.com/science/article/pii/S0929119915001030">https://www.sciencedirect.com/https://www.sciencedirect.com/science/article/pii/S0929119915001030</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/doi/10.5281/zenodo.10423865" target="_blank">https://zenodo.org/doi/10.5281/zenodo.10423865</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Politecnico di Milano
                <a title="view webpage" href="http://www.dig.polimi.it">http://www.dig.polimi.it</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Benedetta Montanaro
                <a title="email to Access Manager" href="mailto: benedetta.montanaro@polimi.it"> benedetta.montanaro@polimi.it </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>

