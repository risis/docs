---
title: EUPRO 
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>EUPRO</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">EUPRO is a unique dataset providing systematic and standardized information on R&amp;D projects, participants and resulting networks of the EU FP, starting from FP1, and recently integrating H2020 (until 2019), and other European funding instruments, such as EUREKA, COST and selected Joint Technology Initiatives (JTIs). It has been recently used intensively as a core facility in research activities that investigate structure, dynamics and impacts of project-based R&amp;D collaboration, in particular to grasp and understand the development of the European Research Area (ERA). Basically, EUPRO covers cleaned and standardised information on R&amp;D projects (such as project objectives and achievements, project costs, total funding, start and end date, contract type, information on the call), and their participants (standardized name of the participating organisation, organisation type, and geographical location). Services currently offered by the infrastructure: EUPRO offers the access to high quality data on R&amp;D projects funded by different European funders. The robustness of the dataset and its relevance for actual research issues of the field has been underlined by the number of transnational access requests and visits conducted in the first RISIS project. EUPRO offers substantial possibilities to address questions on structure and dynamics of knowledge creation, networking patterns of organisations in R&amp;D projects, or impacts of publicly funded R&amp;D, among others. Further, with its huge standardised set of organisational names it offers a great possibility to connect with other organisational level datasets, both within and external to RISIS.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>R&amp;D projects; European Framework Programme; networks; collaborative knowledge production</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1435-5957.2012.00419.x">https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1435-5957.2012.00419.x</a>  <br>
                              <a title="view use case" href=" https://academic.oup.com/joeg/article-abstract/13/1/23/891199?redirectedFrom=fulltext"> https://academic.oup.com/joeg/article-abstract/13/1/23/891199?redirectedFrom=fulltext</a>  <br>
                              <a title="view use case" href=""></a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/7524823#.ZBruSvaZMZU" target="_blank">https://zenodo.org/record/7524823#.ZBruSvaZMZU</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                AIT Austrian Institute of Technology
                <a title="view webpage" href="http://www.ait.ac.at">http://www.ait.ac.at</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Thomas Scherngell
                <a title="email to Access Manager" href="mailto: thomas.scherngell@ait.ac.at"> thomas.scherngell@ait.ac.at </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>