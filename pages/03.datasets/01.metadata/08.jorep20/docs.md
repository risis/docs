---
title: JOREP 2.0
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>JOREP 2.0</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">JoREP is a unique database on European trans-national joint R&amp;D programmes. JOREP stores raw panel data on joint R&amp;D programmes and a basic set of descriptors of agencies participating to the programmes. The actual list of variables was subject to utility and feasibility controls. The current version 2.0, opened in June 2016 (on-site access), covers data for the period 2000-2014, with a specific focus on 2013 and 2014 - assumed as reference years. JOREP 2.0 has a geographical coverage of 32 countries for European-level programmes for the period 2010-2014 and a table of indicators at the country level to allow spatial analysis. Services currently offered by the infrastructure: The facility provides a quantitative basis for the monitoring of investments in joint R&amp;D programmes, pointing out the policy rationales behind them and their impact. Each programme is described by a set of attributes (organisational, financial, etc.). The design of the database has been planned to store data on the amount spent for joint R&amp;D programmes and on their organisational characteristics (managing research funding organisations), according to the analyses of the modes of the ERA dynamics. Combined analyses with EUPRO are currently allowed for JTIs and EUREKA thank to the integration of the two datasets.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>Funding of science; European Research Area; transnational programmes</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://zenodo.org/record/2560373/files/JOREP.pdf?download=1">https://zenodo.org/record/2560373/files/JOREP.pdf?download=1</a>  <br>
                              <a title="view use case" href=" https://academic.oup.com/rev/article-abstract/23/4/312/2889058?redirectedFrom=fulltext"> https://academic.oup.com/rev/article-abstract/23/4/312/2889058?redirectedFrom=fulltext</a>  <br>
                              <a title="view use case" href=" https://www.sciencedirect.com/science/article/abs/pii/S0048733313001911"> https://www.sciencedirect.com/science/article/abs/pii/S0048733313001911</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3530218#.XcKIe9Uxm70" target="_blank">https://zenodo.org/record/3530218#.XcKIe9Uxm70</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                IRCrES-CNR
                <a title="view webpage" href="http://www.ircres.cnr.it">http://www.ircres.cnr.it</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Andrea Orazio Spinello
                <a title="email to Access Manager" href="mailto: andrea.spinello@ircres.cnr.it"> andrea.spinello@ircres.cnr.it </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>