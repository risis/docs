---
title: RISIS Patent 
taxonomy:
  category: docs
---


<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>RISIS Patent</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">RISIS Patent offers an enriched and cleaned version of the PATSTAT database, a database of all patent offices produced by the European patent office (EPO). In RISIS1, three major developments have been made to improve its relevance. The first one deals with geocoding addresses so as to follow the dynamics of invention. A second important development has been taking into account so-called artificial patents (13% of total priority patents). A third development – still at the pilot phase – deals with the recognition of individuals being legal persons in patent owners. Services currently offered by the infrastructure: All these developments will be open and accessible. Users can query it along technological dimensions, geographical dimensions (at country and metropolitan levels), along actors and their agglomeration in sectors. Users can also use the sector-technology concordance map developed by Fraunhofer-ISI as a complementary resource to conduct sectoral studies. In addition, firms (and their subsidiaries) from CinnoB (see above) have been identified and agglomerated data at the firm level is integrated into the CinnoB DB. Identification of public actors (from ORGREG) and of firms from the other datasets (VICO and CHEETAH) has been initiated to insure a greater integration potential for RISIS users.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>patent; invention;</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="http://www.sciences-technologies.eu/images/stories/cv/RISIS_Patents_DB.pdf">http://www.sciences-technologies.eu/images/stories/cv/RISIS_Patents_DB.pdf</a>  <br>
                              <a title="view use case" href=""></a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3342454/files/Documentation_RISIS%20Patstat_Final.pdf?download=1" target="_blank">https://zenodo.org/record/3342454/files/Documentation_RISIS%20Patstat_Final.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Université Paris-Est Marne-la-Vallée (UPEM)
                <a title="view webpage" href="http://www.u-pem.fr/">http://www.u-pem.fr/</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Patricia Laurens
                <a title="email to Access Manager" href="mailto: patricia.laurens@esiee.fr"> patricia.laurens@esiee.fr </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>
