---
title: RISIS-ETER 
taxonomy:
  category: docs
---

<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>RISIS-ETER</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">RISIS-ETER represents an extension of the Tertiary Education Register (ETER; https://www.eter-project.com/) database that is supported by the European Commission through a service contract. RISIS-ETER provides an environment for enriching ETER with additional data in three respects: integrating additional variables from other RISIS datasets, particularly concerning research output (EU-FP participations from EUPRO, scientific publications from CWTS publications databse, patents from IFRIS-PATSTAT), extending the time coverage of ETER, and extending the geographical scope of ETER beyond Europe. RISIS-ETER is closely integrated and hosted by the same technical infrastructure as the register of public-sector research and higher education institutions OrgReg, but is a distinct database providing statistical data.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>higher education; university; register</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://link.springer.com/article/10.1007/s11192-015-1768-2">https://link.springer.com/article/10.1007/s11192-015-1768-2</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://www.eter-project.com/wp-content/uploads/2022/02/ETERIV_Handbook.pdf" target="_blank">https://www.eter-project.com/wp-content/uploads/2022/02/ETERIV_Handbook.pdf</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Università della Svizzera italiana
                <a title="view webpage" href="https://www.usi.ch/en">https://www.usi.ch/en</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Benedetto Lepori 
                <a title="email to Access Manager" href="mailto: blepori@usi.ch"> blepori@usi.ch </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>

