---
title: CHEETAH 
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>CHEETAH</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">The Cheetah dataset (version 3.0) contains geographical, industry, accounting and ownership information on ten cohorts of medium-sized firms that experienced turnover or employment fast growth rates in the periods 2008-11, 2009-12, 2010-13, 2011-14, 2012-15, 2013-16, 2014-17, 2015-18, 2016-19 and 2017-20. These fast growing medium-sized firms (FGMFs) are located in 30 European countries (EU27, Norway, Switzerland and the UK). Mid-sized firms are defined by combining the EUROSTAT definition of medium-sized firm and the French definition of Entreprise de Taille Intermédiaire (ETI). Cheetah covers an overall number of 129,752 firms. The aim of Cheetah is to cover the long-term economic performance of FGMFs, as one of the main pillars of the European industrial and technological system. It is a unique source of data that offers to researchers opportunities to study the growth dynamics of a relevant category of firms (i.e. mid-sized firms), which however has been almost neglected by the current academic literature on firm growth. The European dimension of Cheetah makes the data infrastructure well suited to address research questions concerning the institutional determinants of firm growth and geographical agglomeration effects.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>company; fast-growing; medium-sized; firm; growth</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://zenodo.org/doi/10.5281/zenodo.3520618">https://zenodo.org/doi/10.5281/zenodo.3520618</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/doi/10.5281/zenodo.5146257" target="_blank">https://zenodo.org/doi/10.5281/zenodo.5146257</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Politecnico di Milano
                <a title="view webpage" href="http://www.dig.polimi.it">http://www.dig.polimi.it</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Massimiliano Guerini
                <a title="email to Access Manager" href="mailto: Massimiliano.Guerini@polimi.it"> Massimiliano.Guerini@polimi.it </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>
