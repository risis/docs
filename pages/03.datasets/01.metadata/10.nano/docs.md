---
title: NANO 
taxonomy:
  category: docs
---


<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>NANO</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">The Nano S&T dynamics database (Nano) collects publications and patents between 1991 and 2011 about Nano S&T. One central characteristics of emerging S&T is that they do not correspond to pre-existing categorisations and require the elaboration of semantic based queries. IFRIS has developed a dynamic query gathering 1.18 million Nano-related publications and 735000 priority patents. Services currently offered by the infrastructure: The facility offers harmonised and integrative analysis of Nano S&T dynamics globally, due to four types of enrichments that have been conducted: (i) categorisation and harmonisation of institutional affiliations, (ii) geolocalisation of all authors and inventors; (iii) geographical clustering of S&T activities; and (iv) thematic clustering of S&T activities.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td></td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://sti2017.ifris.org/wp-content/uploads/2017/11/geo-villard-on-nano.pdf?sequence=1">https://sti2017.ifris.org/wp-content/uploads/2017/11/geo-villard-on-nano.pdf?sequence=1</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3338124/files/Documentation_Nano_Final_RISIS1_version.pdf?download=1" target="_blank">https://zenodo.org/record/3338124/files/Documentation_Nano_Final_RISIS1_version.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Université Paris-Est Marne-la-Vallée (UPEM)
                <a title="view webpage" href="http://www.u-pem.fr/">http://www.u-pem.fr/</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Lionel Villard
                <a title="email to Access Manager" href="mailto: lionel.villard@esiee.fr"> lionel.villard@esiee.fr </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>