---
title: CWTS Publication 
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>CWTS Publication Database</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">The CWTS publication database is a full copy of Web of Science (WoS) dedicated to bibliometric analyses. The enhancements and improvement to the original version of WoS involve: harmonized main organisation names, matching cited references to source publications and some other data quality improvements. In this database a permanent link is made between author affiliations and OrgReg identifiers. The database enables output and (scientific) impact analyses of any set of publications covered by WoS, using state of the art methods. Services currently offered by the infrastructure: A public dataset to demonstrate the potential of the database, together with a complete documentation of data and methods used is available at leidenranking.com. In this dataset a set of around 800 universities worldwide is included with their research performance statistics. Through RISIS, a much larger list of organisations will be provided (including PRO’s) with their research performance statistics. More details studies can be executed on-site at CWTS using the underlying database via a research visit.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>academic publishing; journals; articles; web of science</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="http://www.leidenranking.com/downloads">http://www.leidenranking.com/downloads</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3381509/files/Documentation_CWTS_Final_edited.pdf?download=1" target="_blank">https://zenodo.org/record/3381509/files/Documentation_CWTS_Final_edited.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Leiden University
                <a title="view webpage" href="http://www.cwts.nl">http://www.cwts.nl</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Clara Calero Medina
                <a title="email to Access Manager" href="mailto: clara@cwts.leidenuniv.nl"> clara@cwts.leidenuniv.nl </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>