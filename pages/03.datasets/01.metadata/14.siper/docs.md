---
title: SIPER 
taxonomy:
  category: docs
---


<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>SIPER</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">Science and Innovation Policy Evaluations Repository (SIPER) is a rich and unique database and knowledge source of science and innovation policy evaluations worldwide. Effects and efficiency of science, technology and innovation (STI) policies are typically assessed through a process of evaluation and illustrated in the evaluation reports. SIPER provides on-line access to those STI evaluation reports as well as data on the underlying policy measures at a single location. The database allows targeted searches and access to specific instruments and evaluation design for policy practitioners to learn from. It enables policy learning and academic research by providing an informed analysis of the database contents which allows users to pursue a range of research questions on evaluation approaches (methods, designs, objectives etc.), evaluation uses, impact, connection between characteristics of policy design and evaluation design, etc. Services currently offered by the infrastructure: SIPER provides completely new repository of evaluations, with a unique characterisation system, offering an accessible database with a public portal to function as a service to the policy makers and academic communities. SIPER has developed a framework to characterise each evaluation report as well as the corresponding policy measure. SIPER focuses on evaluations after the year 2000. It has a global ambition, but starts with a focus on the Organisation for Economic Co-operation and Development (OECD) member countries/regions. SIPER is open to all interested parties, and is being widely used for policy learning and research purposes. Via RISIS, SIPER offers access to specific data no open in the public version.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>policy evaluations; repository; science policy</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://si-per.eu/siper-en/publications.php">https://si-per.eu/siper-en/publications.php</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3338001/files/RISIS_datasets_documentation_SIPER_v2_rev.pdf?download=1" target="_blank">https://zenodo.org/record/3338001/files/RISIS_datasets_documentation_SIPER_v2_rev.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Fraunhofer ISI
                <a title="view webpage" href="https://www.isi.fraunhofer.de/en.html">https://www.isi.fraunhofer.de/en.html</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Susanne Bührer-Topçu
                <a title="email to Access Manager" href="mailto: Susanne.Buehrer-Topcu@isi.fraunhofer.de"> Susanne.Buehrer-Topcu@isi.fraunhofer.de </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>
