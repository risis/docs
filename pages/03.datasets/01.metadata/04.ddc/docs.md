---
title: Dissertation-Centric Database
taxonomy:
  category: docs
---
<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>Dissertation-Centric Database</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">The Doctoral Degree and Career Dataset (DDC) is and experimental dissertation-centric database that makes two major contributions in this setting. Primarily, the DDC offers the user community an enriched "PhD1 production" dataset. It combines multiple RISIS resources around a core of dissertation metadata. The DDC recruits and integrates richer information about the dissertation (e.g., topic mapping), about the degree-granting university (e.g., geolocation), as well as basic information about the individual (e.g., gender).</td></tr> <tr data-v-abadeeb6=""><td data-v-abadeeb6="" class="property">Keywords</td> <td data-v-abadeeb6="">***KEYWORDS MISSING***</td></tr> <tr data-v-abadeeb6=""><td data-v-abadeeb6="" class="property">Use Cases</td> <td data-v-abadeeb6=""><a data-v-abadeeb6="" href="***USE-CASE MISSING***" target="_blank">***USE-CASE MISSING***</a></td></tr> <tr data-v-abadeeb6=""><td data-v-abadeeb6="" class="property">Documentation</td> <td data-v-abadeeb6=""><a data-v-abadeeb6="" href="https://zenodo.org/record/4446431#.ZBFytXaZNPY" target="_blank">https://zenodo.org/record/4446431#.ZBFytXaZNPY</a></td></tr> <tr data-v-abadeeb6=""><td data-v-abadeeb6="" class="property">Dataset Owner</td> <td data-v-abadeeb6="">***DATASET-OWNER MISSING***</td></tr> <tr data-v-abadeeb6=""><td data-v-abadeeb6="" class="property">Dataset Acces Manager</td> <td data-v-abadeeb6="">Eric Iversen; eric@nifu.no</td></tr> <tr data-v-abadeeb6=""><td data-v-abadeeb6="" class="property">Documentation</td> <td data-v-abadeeb6=""><a data-v-abadeeb6="" href="https://zenodo.org/record/4446431#.ZBFytXaZNPY" target="_blank">https://zenodo.org/record/4446431#.ZBFytXaZNPY</a></td></tr></table></div></div>

