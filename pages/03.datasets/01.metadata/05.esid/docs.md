---
title: ESID
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>ESID </td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">ESID is a comprehensive and authoritative source of information on social innovation projects and actors in Europe and beyond. Initially developed as part of the EU Funded KNOWMAK project, ESID is now being developed as part of the EU funded RISIS 2 Project. ESID utilizes advanced machine learning and natural language processing techniques to collect information about social innovation projects and actors from the publicly available information on the web. ESID also uses some limited human annotation to train its machine learning models and to ensure the quality and the integrity of the data. The ESID database contains two sets of datasets, one being the subset of the other. The full dataset comprises of 9577 social innovation projects in total. The curated dataset which is part of the 9577 projects comprises of data that is high quality as it has been manually checked and annotated by different annotators. For these projects, ESID contains a title, type of social innovation with scores, summary, location and topic.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>Social Innovation, new forms of innovation, social innovation projects, machine learning, Natural Language processing</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://pureportal.strath.ac.uk/en/publications/using-machine-learning-and-text-mining-to-classify-fuzzy-social-s">https://pureportal.strath.ac.uk/en/publications/using-machine-learning-and-text-mining-to-classify-fuzzy-social-s</a>  <br>
                              <a title="view use case" href=" https://link.springer.com/chapter/10.1007%2F978-3-030-23281-8_13"> https://link.springer.com/chapter/10.1007%2F978-3-030-23281-8_13</a>  <br>
                              <a title="view use case" href=" https://link.springer.com/chapter/10.1007%2F978-3-319-91947-8_42"> https://link.springer.com/chapter/10.1007%2F978-3-319-91947-8_42</a>  <br>
                              <a title="view use case" href=" https://www.nature.com/articles/s41597-022-01818-0"> https://www.nature.com/articles/s41597-022-01818-0</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/4605814#.YFBrO537Q2w" target="_blank">https://zenodo.org/record/4605814#.YFBrO537Q2w</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                University of Strathclyde
                <a title="view webpage" href="https://www.strath.ac.uk/">https://www.strath.ac.uk/</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Esra Aydogdu
                <a title="email to Access Manager" href="mailto: esra.aydogdu@strath.ac.uk"> esra.aydogdu@strath.ac.uk </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>