---
title: ISI-TM
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>ISI-Trademark Data Collection (ISI-TM)</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">The ISI-Trademark Data Collection (ISI-TM) provides detailed information on trademarks filed at the EUIPO and at the USPTO. It includes information on all trademarks filed at the EUIPO since 1996 and at the USPTO since the early 19th century onwards. The data provided are the trademark number, as a unique identifier, applicant and representative information, several relevant dates application, registration,...), trademark type (word, figurative, sound,…), the language of the trademark, information on opposition and information on relevant classifications. The trademark information is processed and set-up into an Oracle relational database system (RDBMS). The ISI-Trademark Data Collection (ISI-TM) offers using trademarks as a complementary and relatively "close to the market" indicator for new products and innovation activities, especially in the service sector. It offers the possibility to be linked to other datasets at the organizational level, within the RCF as well as externally. </p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>Trademarks, IPR, EUIPO, USPTO</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href=""></a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/4633061#.YFsiRNwxmUk" target="_blank">https://zenodo.org/record/4633061#.YFsiRNwxmUk</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                Fraunhofer Institute for Systems and Innovation Research ISI
                <a title="view webpage" href="https://www.isi.fraunhofer.de/en.html">https://www.isi.fraunhofer.de/en.html</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Peter Neuhäusler
                <a title="email to Access Manager" href="mailto: peter.neuhaeusler@isi.fraunhofer.de"> peter.neuhaeusler@isi.fraunhofer.de </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>