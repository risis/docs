---
title: MORE 
taxonomy:
  category: docs
---



<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>MORE</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">MORE (Mobility Survey of the Higher Education Sector) is arguably the most comprehensive empirical study of researcher mobility available. The MORE dataset ('facility') is composed of three independent waves of extensive, Europe-wide surveys in this family (MORE1, MORE2 and MORE3). They were carried out on behalf of the EU Commission among large samples of researchers at European universities, respectively. MORE provides a unique lens on mobility patterns and career paths in Europe, including measures of flows of international (and sector) mobility, of factors that influence mobility, and of effects that can be linked to researcher mobility. Services currently offered by the infrastructure: The MORE facility includes harmonization (e.g. of field of science, career stages) and links the data to the European Tertiary Education Register (RISIS-ETER). The linked information includes variables such as the distribution of staff by field of science, overall funding, and share of foreign students or staff, and provides the scope for in-depth analysis as it allows the research community to control for important institutional-level effects.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>researcher mobility; research careers; survey; career stages</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://nifu.brage.unit.no/nifu-xmlui/bitstream/handle/11250/2358894/NIFUarbeidsnotat2014-11.pdf?sequence=1&isAllowed=y?sequence=1">https://nifu.brage.unit.no/nifu-xmlui/bitstream/handle/11250/2358894/NIFUarbeidsnotat2014-11.pdf?sequence=1&isAllowed=y?sequence=1</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3337987/files/Documentation_More_Final_edited.pdf?download=1" target="_blank">https://zenodo.org/record/3337987/files/Documentation_More_Final_edited.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                NIFU
                <a title="view webpage" href="http://www.nifu.no/en/">http://www.nifu.no/en/</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Eric Iversen
                <a title="email to Access Manager" href="mailto: eric@nifu.no"> eric@nifu.no </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>