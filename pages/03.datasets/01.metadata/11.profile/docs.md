---
title: PROFILE 
taxonomy:
  category: docs
---

<div class="col-sm rcf-col-middle"> 
      <br>
        <table class="table">
                    <tbody><tr>
            <td width="220px"><strong>Title</strong></td>
            <td>PROFILE</td>
          </tr>
          <tr>
            <td><strong>Description</strong></td>
            <td><p style="text-align: justify;">ProFile is a longitudinal study focusing on the situation of doctoral candidates and their postdoctoral professional careers. The study started in 2009 and continued until 2017 when it was dissolved in the successor study Nacaps. Up to date, ProFile and its successor study Nacaps are the only longitudinal studies in Europe containing doctoral students from all scientific disciplines and regular information on the conditions of their doctoral training. ProFile is based on a systematic sample of doctoral candidates at universities and funding organisations in Germany. ProFile has seen visits during RISIS partly from researchers active in collecting data on careers themselves. ProFile and Nacaps thus serve as a beacon for similar data collection endeavours in other countries. The visits advance research on careers of PhDs and help build a network of data collectors on PhD careers in Europe.</p></td>
          </tr>
          <tr>
            <td><strong>Keywords</strong></td>
            <td>researcher mobility; research careers; survey; career stages</td>
          </tr>
          <tr> 
            <td><strong>Use Cases</strong></td>
             <td>
                              <a title="view use case" href="https://doi.org/10.1093/reseval/rvx024">https://doi.org/10.1093/reseval/rvx024</a>  <br>
                              <a title="view use case" href=" https://www.degruyter.com/view/j/jbnst.2017.237.issue-4/jbnst-2015-1037/jbnst-2015-1037.xml"> https://www.degruyter.com/view/j/jbnst.2017.237.issue-4/jbnst-2015-1037/jbnst-2015-1037.xml</a>  <br>
                            </td>
          </tr>
          <tr> 
            <td><strong>Documentation</strong></td>
             <td>
              <a href="https://zenodo.org/record/3337989/files/Documentation_ProFile_Final_edited.pdf?download=1" target="_blank">https://zenodo.org/record/3337989/files/Documentation_ProFile_Final_edited.pdf?download=1</a>
              </td>
          </tr>
          <tr>
            <td><strong>Dataset Owner</strong></td>
            <td>
                DZHW
                <a title="view webpage" href="https://www.dzhw.eu/">https://www.dzhw.eu/</a>
            </td>
          </tr>
          <tr>
            <td><strong>Dataset Access Manager</strong></td>
              <td>
                Jakob Tesch 
                <a title="email to Access Manager" href="mailto: tesch@dzhw.eu"> tesch@dzhw.eu </a>
              </td>
          </tr>
        </tbody></table>
       <hr>
      </div>

