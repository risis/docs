---
title: CInnoB Extraction
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The "CInnoB Extraction" scenario comprises a set of finely tuned processes designed to systematically gather and extract specific data from the risis2 CInnoB dataset API. It targets various dimensions of information related to firm-level indicators within CinnoB. Whether focusing on financial data, trademarks, patents, or specific domains, each extraction command serves a unique purpose in facilitating the retrieval of granular insights.</p>

- <p style="text-align: justify;">This modular approach allows researchers and analysts to tailor their data extraction efforts to specific areas of interest, providing a versatile and customizable toolkit for exploring the intricate dynamics of knowledge production, innovation, and corporate activities within large firms over specified timeframes and domains.</p>

## Steps

- Upon logging in, start a new project and proceed to import the "CInnoB Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![cinnob-extraction-scenario.png](cinnob-extraction-scenario.png?cropResize=800)

  - **Year:** Select the range of years that you want to analyze.
  - **ISO2 country:** 	Add multiples iso countries like FR.
  - **Firm sector nace3:** 0-99 is the range of all the sectors existing in the database.
  - **Maximal number of records to return:** Upper limit on the number of records returned by the search criteria. For this example, we select '1000'.
  - **Dataset title:** Enter a title for the dataset to be created. This title will help to identify and use the data in projects. For this example, we select the file 'My CinnoB Dataset'. 
  - **User e-mail:** 	Enter the email address of the user to grant access to the generated extraction. Leave blank if you don't want to share it.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced.
