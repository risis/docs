---
title: CIB Firms Extraction
taxonomy:
  category: docs
---

## Context

- CIB scenario fetches from the RISIS2 CIB dataset API. You will get as result a tsv file per each of the following items,  filtered by the firm register ID you input, which is included in all of the output tables
  - FirmAddress
  - FirmFinancialData
  - FirmNames
  - FirmSector
  - Firms
  - GuoPatentsActor
  - PatentsActors

- The Query Factory for the CIB extraction provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on cib, such as keywords, grant dates, and other filters.

## Steps

- Upon logging in, start a new project and proceed to import the "CIB Firms Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![cib-firms-extraction-scenario.png](cib-firms-extraction-scenario.png?cropResize=800)


  - **Search criteria:** Use the query factory to customize your search criteria.
  - **Maximal number of records to return:** Use this field to constrains the number of records returned.
  - **Dataset title:** Enter the dataset title to where files will be uploaded. For this example, we select the file 'My CIB Dataset'.
  - **User e-mail:** 	E-mail of the user that should be given full access to the extraction.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced.
