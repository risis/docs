---
title: EUPRO Project Info
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The "EUPRO Project Info" scenario, which provides systematic and standardized information on R&D projects of different European R&D policy programmes. In this case, the detailed information related to one single project. It provides the basic information about an specific R&D project as well as its related publications, programmes, reports and deliverables.</p>

- <p style="text-align: justify;">This scenario, by virtue of its modular structure, facilitates researchers, project managers, and analysts in obtaining a comprehensive understanding of EU-funded research initiatives, promoting transparency and in-depth analysis across various dimensions of project execution and outcomes.</p>

## Steps

- Upon logging in, start a new project and proceed to import the "EUPRO Project Info" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![eupro-project-info-scenario.png](eupro-project-info-scenario.png?cropResize=800)
  - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Title for the new dataset:** Enter the dataset title to where files will be uploaded. For this example, we selected 'EUPRO Project Info Test'.
  - **Project Id:** Enter the project id of the project you are interested in. For this example, we selected '200853'.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced: `Information of the requested EUPRO project by the id '200853'`, `Publications of the requested EUPRO project by the id '200853'`, `Programmes of the requested EUPRO project by the id '200853'`, `Reports of the requested EUPRO project by the id '200853'` and `Deliverables of the requested EUPRO project by the id '200853'`.
