---
title: RPD Extraction
taxonomy:
  category: docs
---

## Context
- <p style="text-align: justify;">The "RPD Extraction" scenario provides a way to easily query and produce sub-dataset from the RISIS Patent Database. The scenario is designed to allow to search through the millions of patents with ease, giving the ability to create customized sub-dataset. Once the data is extracted it can be analyzed using other scenarios or be downloaded and used with other tools. </p>

- <p style="text-align: justify;">The Query Factory for the RISIS Patent Database provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on efil, such as keywords, grant dates, and other filters.</p>


## Steps

- Upon logging in, start a new project and proceed to import the "RPD Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![rpd-extraction-scenario.png](rpd-extraction-scenario.png?cropResize=800)

  - **Search criteria:** Use the query factory to customize your search criteria. For this example, we wanted the earliest_filing_year to be greater than '2012'.
  - **Maximal number of records to return:** Use this field to constrains the number of records returned. For this example, we set the bar at '1000'.
  - **Dataset title:** Enter the title for the new dataset. For this example, we selected 'My Patent Dataset'.
  - **User e-mail:** Enter user's email to share the extraction with.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced: `Patents Basic Info Extraction Result`, `Patent Technical Classification Info Extraction Result`, `Patent Actors Extraction Result`, `Patent Inventors Info Extraction Result` and `Patents Full Info Extraction Result`.
