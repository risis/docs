---
title: Linking OrgReg or CIB data with RPD
taxonomy:
  category: docs
---

## Context

- The OrgReg (Register of Public-Sector Organizations) is a central facility within the EU-FP Research Infrastructure for Research and Innovation Studies project (RISIS) providing tools for the harmonization of actors in the public research system. Complementarily, RISIS is currently developing the prototype of a parallel register for firms (FirmReg). -> [website](https://register.orgreg.joanneum.at/).

- The CIB (Corporate Invention Board) dataset is a database characterising the patent portfolios of the largest industrial firms worldwide. The CIB combines information extracted from the Industrial R&D Investment Scoreboard (EU Commission), the ORBIS financial database and the RISIS Patent database ( enriched version of the Patstat EPO database). -> [Gitlab repo](https://gitlab.com/cortext/patents/cib-database).

## Steps

- Upon logging in, start a new project and proceed to import the "Linking OrgReg or CIB data with RPD" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![linking-orgreg-cib-rpd-scenario.png](linking-orgreg-cib-rpd-scenario.png?cropResize=800)
  - **RISIS dataset:** CIB or ORGREG depending on the data file you will select. For this example we choose ORGREG.
  - **Dataset:** Select one dataset from the ones available in the list (the datasets already imported into the project). For this example, we select the dataset 'ORGREG TEST' which was the result of the scenario 'ORGREG'.
  - **File:** Select one **tsv** file from the previously selected dataset, which contains the ORGREG (or CIB) entities data. For this example, we select the file 'orgreg-entities-result-1.tsv'.
  - **Fieldname containing entity id:** Write the column name of the selected file that contains the entity id to link with RPD. 
    - Commonly for OrgReg it will be `entities.BAS.ENTITYID.v` and for CIB `firmreg_id`, but you can modify them to match with your customized file.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced: `Patent Actors Extraction Result`, containing all the legal patent applicants linked to the entity id values of your source file.
