---
title: CHEETAH Extraction
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">This scenario provides a way to easily query and produce sub-corpora from the RISIS Cheetah dataset. The Cheetah dataset contains geographical, industry, accounting and ownership information on seven cohorts of medium-sized firms that experienced turnover or employment fast growth rates in the periods 2008-11, 2009-12, 2010-13, 2011-14, 2012-15, 2013-16, and 2014-17.</p>

- <p style="text-align: justify;">The Query Factory for the Cheetah Database provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on cheetah, such as keywords, grant dates, and other filters.</p>

## Steps

- Upon logging in, start a new project and proceed to import the "Cheetah Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![cheetah-extraction-scenario.png](cheetah-extraction-scenario.png?cropResize=800)

  - **Search criteria:** Use the query factory to customize your search criteria.
  - **Maximal number of records to return:** Use this field to constrains the number of records returned.
  - **Dataset title** Enter the title for the new dataset. For this example, we selected 'My Cheetah Dataset'.
  - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced.
