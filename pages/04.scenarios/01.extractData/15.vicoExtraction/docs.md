---
title: VICO Extraction
taxonomy:
  category: docs
---

## Context

- VICO scenario fetches from the RISIS2 VICO dataset API. You will get as result a tsv file per each of the following items, filtered by the year you input. And this year corresponds in each table to the column specified below.
  - Companies: CompanyFoundedYear
  - Investments: InvestmentYear
  - Investors
  - Accounting: Year
  - Equity CF Investments: InvestmentYear
  - Exits: AcqYear

- The Query Factory for the Vico Database provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on vico, such as keywords, grant dates, and other filters.

## Steps

- Upon logging in, start a new project and proceed to import the "VICO Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![vico-extraction-scenario.png](vico-extraction-scenario.png?cropResize=800)

  - **Search criteria:** Use the query factory to customize your search criteria.
  - **Maximal number of records to return:** Use this field to constrains the number of records returned.
  - **Dataset title:** Enter the dataset title to where files will be uploaded. For this example, we selected 'My Vico Dataset'.
  - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced.
