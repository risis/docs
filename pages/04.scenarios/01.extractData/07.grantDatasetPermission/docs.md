---
title: Grant Dataset Permission
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The "Grant Dataset Permission" scenario involves managing and granting dataset permissions. It is designed to verify the existence of a user, ensuring accurate identification within the system. Subsequently, it plays a crucial role in assigning specific roles to users, enabling them to access and interact with designated datasets. This scenario is essential for administrators and data managers seeking to regulate and control access to datasets, ensuring that only authorized.</p>


## Steps

- Upon logging in, start a new project and proceed to import the "Grant Dataset Permission" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![grant-dataset-permission-scenario.png](grant-dataset-permission-scenario.png?cropResize=800)
  - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Dataset Id:** Select the Dataset you want to give access to.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, the dataset permission is granted.
