---
title: OrgReg
taxonomy:
  category: docs
---

## Context

- The "Orgreg" scenario is designed for extracting data from the OrgReg database which contains the register of public research and higher education organizations [https://register.orgreg.joanneum.at/#/search/sub-register/0](https://register.orgreg.joanneum.at/#/search/sub-register/0). 

## Steps

- Upon logging in, start a new project and proceed to import the "OrgReg" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![orgreg-scenario.png](orgreg-scenario.png?cropResize=800)
	
  - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Title for the new dataset:** Enter the dataset title to where files will be uploaded. For this example, we selected 'ORGREG TEST'.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.  

- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced: `OrgReg Entities Result`, `OrgReg Characteristics Result`, `OrgReg Locations Result`, `OrgReg Demographics Result` and `OrgReg Linkages Result`.