---
title: NANO Documents Extraction
taxonomy:
  category: docs
---

## Context
- NANO scenario obtains the data from the RISIS2 NANO dataset API. You will get as result a tsv file with all the NANO documents filtered by the available inputs. The following are the info you will get for each document:
  - id: Id of the publication
  - document_title: Title of the publication
  - document_abs: Abstract of the publication
  - doc_pub_year: Year of publication
  - publication: Journal of the publication
  - publication_aiso: Journal name normalized
  - publication_issn: ISSN Number of the publication
  - doc_pub_volu: Volume
  - doc_pub_issue: Issue
  - doc_pub_bpag: Begin page
  - doc_pub_epag: End page
  - doc_pub_npag: Number of pages


## Steps

- Upon logging in, start a new project and proceed to import the "NANO Documents Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![nano-documents-extraction-scenario.png](nano-documents-extraction-scenario.png?cropResize=800)

  - **User e-mail:** E-mail of the user that should be given full access to the extraction
  - **Title for the new dataset:** Enter the dataset title to where files will be uploaded. For this example, we selected 'Test Nano'.
  - **Year of publication:** Enter a year you're interested in. For this example, we selected '2008'.
  - **Journal name normalized:** Enter the Journal name normalized you are interested in. For this example, we selected 'Intermetallics'.
  - **Number of pages:** Enter the number of pages you are interested in. For this example, we selected the number '10'.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced: `nano-documents-result`.
