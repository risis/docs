---
title: EFIL Extraction
taxonomy:
  category: docs
---

## Context

- EFIL scenario fetches from the RISIS2 EFIL dataset API. You will get as result a tsv file for each of the following items:
  - Country: country
  - Identifier for the RFO: rfo_id (Same ID as in OrgReg)
  - Acronym of the RFO: rfo_acronym
  - Name of RFO in English language: rfo_name
  - RFO domain: rfo_domain
  - Instrument name in original language: instrument_name_original_lang;
  - Instrument name in English: instrument_name_in_english
  - Instrument aim: instrument_aim
  - Integration with NATPRO: NATPRO_code
  - Integration with SIPER: SIPER_code

- The Query Factory for the Efil Database provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on efil, such as keywords, grant dates, and other filters.

- The scenario application enables the user to discover the portfolio of R&amp;D instruments of the national
RFOs included in the EFIL perimeter. Furthermore, it allows verifying the presence of interlinking with
OrgReg, the NATPRO module of EUPRO, and SIPER.


## Steps

- Upon logging in, start a new project and proceed to import the "EFIL Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![efil-extraction-scenario.png](efil-extraction-scenario.png?cropResize=800)

  - **Search criteria:** Use the query factory to customize your search criteria.
  - **Maximal number of records to return:** Use this field to constrains the number of records returned.
  - **Dataset title** Enter the title for the new dataset. For this example, we selected 'My Efil Dataset'.
  - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new file produced.
