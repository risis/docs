---
title: EUPRO Projects and Participations
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The "EUPRO Projects and Participations" scenario provides systematic and standardized information on R&D projects of different European R&D policy programmes and information about the participants, which can be used to analyze networks and collaborative knowledge production, providing a comprehensive view of collaborations and partnerships within the EU research landscape. </p>

## Steps

- Upon logging in, start a new project and proceed to import the "EUPRO Projects and Participations" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![eupro-projects-and-participation-scenario.png](eupro-projects-and-participation-scenario.png?cropResize=800)

  - **Title for the new dataset:** Enter a name for the new dataset. For this example, we selected 'EUPRO Projects and Participations'.
  - **Keyword to search:** Write the keyword you wish to search for. For this example, we selected the keyword 'internet'.
  - **From year:** Write the year you wish to start from. For this example, we selected '2015'.
  - **Until year:** Write the year you wish to stop at. For this example, we selected '2016'.
  - **Country:** Choose the country you are interrested in. For this example, we selected 'IT' which stands for Italy.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced: `EUPRO Projects Result` and `EUPRO Participations Result`.
