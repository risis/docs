---
title: ISI-TM Extraction
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The ISI-Trademark Data Collection "(ISI-TM) extraction" scenario involves the precise extraction of trademark numbers, applicant and representative details, crucial application and registration dates, trademark types, language specifications, opposition information, and relevant classifications.</p>

- <p style="text-align: justify;">The Query Factory for the ISI-TM Database provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on vico, such as keywords, grant dates, and other filters.</p>




## Steps

- Upon logging in, start a new project and proceed to import the "ISI-TM Extraction" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![isi-tm-extraction-scenario.png](isi-tm-extraction-scenario.png?cropResize=800)

  - **Search criteria:** The Query Factory for the ISI-TM Database provides a user-friendly tool for building custom search queries. It allows to easily specify search criterias on ISI-TM , such as keywords, grant dates, and other filters.
  - **Maximal number of records to return:** Upper limit on the number of records returned by the search criteria
  - **Dataset title:**
  Enter a title for the dataset to be created. This title will help to identify and use the data in projects
  - **User e-mail:** Enter the email address of the user to grant access to the generated extraction. Leave blank if you don't want to share it.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, the dataset permission is granted.
