---
title: Patents Extraction for Cortext
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The "Patents Extraction for Cortext" scenario is designed for extracting data from the RISIS Patent database. It specifically prepares the extracted data for uploading to Cortext. It includes various sections, such as basic data, value data, technological classification data, textual information data, actors data (applicants), inventors data, and more. It defines parameters for filtering patents based on different criteria, such as filing year, application ID, office of application, grant year, and various value indicators. </p>

## Steps

- Upon logging in, start a new project and proceed to import the "Patents Extraction for Cortext" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![patent-extraction-for-cortext-scenario.png](patent-extraction-for-cortext-scenario.png?cropResize=800)

    - **User e-mail:** E-mail of the user that should be given full access to the extraction.
  - **Dataset:** We need to select one of the datasets. For our case, we will select the dataset imported previously 'EUPRO Projects and Participations'.

  ![patent-extraction-for-cortext-scenario1.png](patent-extraction-for-cortext-scenario1.png?cropResize=800)

  - **Application id:** Enter only integer numbers. Press enter to add it. For this example, we selected '58' and '73'.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced:`Patents Extraction Result`.
