---
title: IPC Analysis
taxonomy:
  category: docs
---

## Context

- <p style="text-align: justify;">The "IPC analysis" scenario is designed to analyze the recombination of IPC (International Patent Classification) classes, acting as a proxy for the patent technological base. This scenario explores the evolution of IPC classes over time, detecting main recombination of technologies, clustering them, and tracking their evolution. It utilizes datasets provided by the user, specifying the dataset and file IDs. Users can further refine their analysis by selecting a specific year span and target IPC class symbols. It interfaces with Cortext for parsing, term extraction, and network mapping. The output includes interactive Geo Explorer maps, CSV files detailing countries, regions, urban and rural areas, as well as various files related to term extraction and network mapping. The generated reports offer insights into the evolving patent landscape, making it a valuable tool for patent analysts and researchers.</p>

## Steps

- Upon logging in, start a new project and proceed to import the "IPC Analysis" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![ipc-analysis-scenario.png](ipc-analysis-scenario.png?cropResize=800)
  ![ipc-analysis-scenario1.png](ipc-analysis-scenario1.png?cropResize=800)
  - **Dataset id:** Select a dataset that you have previously uploaded in this project.
  - **File id:** Select a file from the dataset you have seleted in the previous field.
  - **Year span:** Select the range of years that you want to analyze. For this example, we selected the years from '2000' to '2017'.
  - **IPC class symbol:** Select a set of International Patent Classification (IPC) ids that you want to analyze.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced.
