---
title: Cortext Geospatial Semantic Analysis
taxonomy:
  category: docs
---

## Context

- This scenario provides a way to load a WoS database that contains among its fields: Abstract, ISIID, Keywords, Title, Address
- Run the following scripts in Cortext Manager: 
  - Data parsing 
  - Terms extraction 
  - Network Mapping 
  - Geocoding script 
  - Geospatial Exploration 
  - Contingency Matrix

- It will output the result of the scripts as csv files or links to display a map generated in an external web tool.

## Steps

- Upon logging in, start a new project and proceed to import the "Cortext Geospatial Semantic Analysis" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![cotext-geospatial-semantic-analysis-scenario0.png](cotext-geospatial-semantic-analysis-scenario0.png?cropResize=800)
  - **Dataset id:** Select a dataset that you have previously uploaded in this project.
  - **File id:** Select a file from the dataset you have seleted in the previous field.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced.
  ![cotext-geospatial-semantic-analysis-scenario1.png](cotext-geospatial-semantic-analysis-scenario1.png?cropResize=800)
  This network is made through CorTexT Manager's "Network Mapping" script. This script facilitates the visualization and organization of network components, enabling users to understand and manage their network structures effectively [see more](https://docs.cortext.net/analysis-mapping-heterogeneous-networks/).
  ![cotext-geospatial-semantic-analysis-scenario2.png](cotext-geospatial-semantic-analysis-scenario2.png?cropResize=800)
  This map has been generated using Cortex Manager's "Geospatial Exploration" script, which offers advanced tools and functionalities for analyzing and visualizing geographical data, allowing users to gain valuable insights into spatial patterns, geographical trends, and location-based information [see more](https://docs.cortext.net/cortext-geospatial-exploration-tool/).
  ![cotext-geospatial-semantic-analysis-scenario3.png](cotext-geospatial-semantic-analysis-scenario3.png?cropResize=800)
  This matrix was created using Cortex Manager's Contingency Matrix, a powerful tool for analyzing and visualizing relationships and dependencies between variables or factors within datasets [see more](https://docs.cortext.net/contingency-matrix/).