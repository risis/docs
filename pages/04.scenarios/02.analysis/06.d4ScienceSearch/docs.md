---
title: D4Science Search
taxonomy:
  category: docs
---

## Context
- <p style="text-align: justify;">The "D4Science search" scenario is designed for querying the D4Science RISIS dataset. It allows users to conduct searches using specified criteria through the Query Factory. It fetches items from the RISIS D4Science dataset and compiles them into a CSV file. Users can customize their search by utilizing the user-friendly Query Factory, specifying keywords, grant dates, and other filters. The resulting dataset is given a user-defined title, aiding in identification and usage in projects. It facilitates seamless interaction with the D4Science RISIS dataset, making it a valuable tool for researchers and data analysts.</p>

## Steps

- Upon logging in, start a new project and proceed to import the "D4Science Search" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![d4science-search-scenario.png](d4science-search-scenario.png?cropResize=800)
  - **Search criteria:** The Query Factory provides a user-friendly tool for building custom search queries. It allows to easily specify search criteria, such as keywords, grant dates, and other filters.
  - **Dataset title:** Enter a title for the dataset to be created. This title will help to identify and use the data in projects
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced.
