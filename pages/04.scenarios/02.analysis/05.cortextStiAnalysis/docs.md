---
title: Cortext STI analysis
taxonomy:
  category: docs
---

## Context

- Science and Innovation corpus (patents/publication) analysis scenario.

  - **Geoexploration**: Interactive mappings, are there subjects areas of specialization in  different urban areas? ([topic] x [geo])

  - **Terms extraction**: Automated process of identifying and extracting relevant keywords and phrases from unstructured text data, enabling enhanced content analysis and information retrieval

  - **Network Mapping**: Visual representation of data relationships and connections, empowering users to gain valuable insights into complex data ecosystems 

## Steps

- Upon logging in, start a new project and proceed to import the "Cortext STI analysis" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![cortext-sti-analysis-scenario.png](cortext-sti-analysis-scenario.png?cropResize=800)
  - **Dataset id:** Select a dataset that you have previously uploaded in this project.
  - **File id:** Select a file from the dataset you have seleted in the previous field.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.

- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced.

![cortext-sti-analysis-scenario1.png](cortext-sti-analysis-scenario1.png?cropResize=800)
- Network Mapping analysis #1: How the subjects areas of specialization are co-occurring depending on  the nature of the produced knowledge? ({ [topic] x [topic] } | [entity])

![cortext-sti-analysis-scenario2.png](cortext-sti-analysis-scenario2.png?cropResize=800)
- Network Mapping analysis #2: How the different types of entities (Projects, Patents, Publications,  Thesis) have an impact in subjects? ([entity] x [topic])

![cortext-sti-analysis-scenario3.png](cortext-sti-analysis-scenario3.png?cropResize=800)
  - Network Mapping analysis #3: What types of actors and what specific subjects are mobilized according  to the nature (Patent, Publication, Project, Thesis) of the knowledge  produced? ({ [topic] x [actor]} | [entity])

![cortext-sti-analysis-scenario4.png](cortext-sti-analysis-scenario4.png?cropResize=800)
- Network Mapping analysis #4: Who are the main actors and how do they cooperate depending on the  nature of the produced knowledge? ({[actor] x [actor]} | [entity])

![cortext-sti-analysis-scenario5.png](cortext-sti-analysis-scenario5.png?cropResize=400)
- Network Mapping analysis #5: Who are the main actors identified in the OrgReg and CIB2 registers  and how do they cooperate according to the nature of the knowledge  produced? ({ [actor] x [actor]} | [entity])

![cortext-sti-analysis-scenario6.png](cortext-sti-analysis-scenario6.png?cropResize=800)
- Network Mapping analysis #6: How are the co-collaboration networks structured between the main  actors according to their geographical location? ({[actor] x  [actor]} | [geo])

![cortext-sti-analysis-scenario7.png](cortext-sti-analysis-scenario7.png?cropResize=800)
- Network Mapping analysis #7: How the different types of organizations (Academic, Company,  Research, Health, …) have an impact in subjects? ([actor] x [topic])

![cortext-sti-analysis-scenario8.png](cortext-sti-analysis-scenario8.png?cropResize=800)
- Network Mapping analysis #8: Homogeneous network of the 250 extracted terms; Distribution top  250; Louvain Resolution; Display of described-entity-type in  Variable with Raw