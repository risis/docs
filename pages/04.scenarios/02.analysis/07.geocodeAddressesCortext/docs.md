---
title: Geocode Addresses (CorTexT)
taxonomy:
  category: docs
---

## Context
- This scenario provides a way to load a csv file containing a column with  address information. It runs the following scripts in Cortext Manager: 
  - Data parsing  
  - Geocoding script
- It will output the result of the Geocoding script as a csv file with the  longitude and latitude values for each address.

## Steps

- Upon logging in, start a new project and proceed to import the "Geocode Addresses (CorTexT)" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![geocode-addresses-cortext-scenario1.png](geocode-addresses-cortext-scenario1.png?cropResize=800)
  ![geocode-addresses-cortext-scenario2.png](geocode-addresses-cortext-scenario2.png?cropResize=800)
  - **Dataset id:** Select a dataset that you have previously uploaded in this project.
  - **File id:** Select a file from the dataset you have seleted in the previous field.
  - **File format:** Select a file format. It could be tab separated or standard csv separated by ; and minimal quoting.
  - **Field with addresses:** Select a field name containing addresses.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced.
