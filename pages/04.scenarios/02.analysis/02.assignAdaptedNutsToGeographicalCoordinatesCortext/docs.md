---
title: Assign adapted nuts to geographical coordinates (CorTexT)
taxonomy:
  category: docs
---

## Context
- This scenario provides a way to load a csv file containing two columns  containing coordinates: longitude and latitude. It runs the following scripts in Cortext Manager:  
  - Data parsing 
  - Geospatial Exploration
- It will produce a way to visualize the results in a web mapping tool.

## Steps

- Upon logging in, start a new project and proceed to import the "Assign adapted nuts to geographical coordinates (CorTexT)" scenario. Next, configure the scenario by filling in the necessary inputs:
  ![assign-adapted-nuts-to-geographical-coordinates-cortext0.png](assign-adapted-nuts-to-geographical-coordinates-cortext0.png?cropResize=800)

  - **Dataset id:** Select a dataset that you have previously uploaded in this project.
  - **File id:** Select a file from the dataset you have seleted in the previous field.
  - **Field with longitude:** Select the field label denoting longitude.
  - **Field with latitude:** Select the field label denoting latitude.
  - **File format:** Select a file format. It could be tab separated or standard csv separated by ; and minimal quoting.
  - **Execution title:** Enter an execution title to categorize each scenario configuration execution. This field is optional.
  
- When all the required fields are filled, you can click on the button in the top right ‘Run this scenario’.
- After the scenario execution is finished, check in the Outputs of the project for the new files produced.
  ![assign-adapted-nuts-to-geographical-coordinates-cortext1.png](assign-adapted-nuts-to-geographical-coordinates-cortext1.png?cropResize=800)
  This map has been generated using Cortex Manager's "Geospatial Exploration" script, which offers advanced tools and functionalities for analyzing and visualizing geographical data, allowing users to gain valuable insights into spatial patterns, geographical trends, and location-based information [see more](https://docs.cortext.net/cortext-geospatial-exploration-tool/).