---
title: "ADDITIONAL HELP"
linkless: true
icon: additional-help.svg
#class: linkless
taxonomy:
  category: docs
---

This FAQ is intended to provide useful answers to common questions.

<style>
  .title {
    font-weight: bold;
    cursor: pointer;
    font-size: 1.5em;
  }
  summary {
    display: list-item !important;
  }
</style>
<details>
  <summary><span class="title">What is RISIS 2?</span></summary>
  RISIS 2 is a continuation of the RISIS project (Research Infrastructure for reSearch and Innovation policy Studies), an infrastructure for researchers that provides databases and services to analyze them, around 5 specific themes:

  <ul>
    <li>
      Innovation dynamics of firms (large global firms, start-ups supported by venture capital in Europe, medium-sized high-growth firms) 
    </li>
    <li>European integration</li>
    <li>Knowledge dynamics (scientific and technical production - publications, patents, trademarks - and European and national research projects)</li>
    <li>Careers of doctoral students and researchers and mobility</li>
    <li>Support public decision making (knowledge of research and innovation policies, evaluation of policies and their instruments at the global level, funding of programs covering all of Europe) RISIS 2 has 4 new missions:</li>
    <ul>
      <li>
        To develop a digital platform allowing remote access to researchers
      </li>
      <li>To provide an expanded set of services adapted to the specific needs of the field</li>
      <li>Preserve and update all its databases</li>
      <li>Produce 4 new databases concerning research funding agencies, social innovation projects and actors, non-academic postdoctoral careers, and brands.</li>
    </ul>
  </ul>

RISIS 2 will therefore add 4 new databases to the 12 already existing ones, 3 of which will be highly developed. These 16 databases will be interoperable, i.e. they will be able to interact with each other, share their information in order to bring out new study perspectives, all via 3 tools:

  <ul>
    <li>A European register of research and higher education organizations</li>
    <li>A semantic and geolocation tool</li>
    <li>An ontological tool for thematic research</li>
  </ul>

In RISIS 2, the effort is focused on the development of services that allow the integration of databases and their processing. The emphasis in processing is on semantic analysis and advanced quantitative methods.

</details>

<details>
  <summary><span class="title">What is the project description?</span></summary>

RISIS2 Project like the RISIS CORE FACILITY (RCF), is organized around 3 major dimensions and activities:

  <p>1. A front end, focusing on users, the ways they access RISIS, work within RISIS, and build RISIS user communities. At the core is the RISIS Core facility (WP4). The core facility supports virtual transnational access (WP8) and is accompanied by all the efforts we do to raise awareness, train researchers, interact with them (WP2), and help them build active user communities (mobilizing D4Science VRE, WP7).</p>

  <p>2. A service layer that helps users organize problem-based integration of RISIS datasets (with possibilities to complement their own datasets) – this entails the data integration and analysis services (WP5) and methodological support for advanced quantitative methods (WP6).</p>

  <p>3. A data layer that gathers the core RISIS datasets that we maintain (WP5) and enlarge (WP9), the datasets of interest for which we ensure reliability and harmonization for integration (WP4), and the new datasets that we develop and will progressively open (WP10).</p>

This is complemented by transversal activities – management both operational (WP1) and strategic (WP3) supported by ethical reviews (WP11), communication, and dissemination & exploitation activities (WP2).

</details>
<details>
  <summary><span class="title">What is RCF?</span></summary>
  The objective of the Risis Core Facility is to provide a ground-breaking infrastructure for Science, Technology, and Innovation (STI) studies. In order to get a functional Risis Core Facility (RCF), the infrastructure must provide a unique entry point online, with which RISIS users can access a monitored and secured workspace. This workspace will be designed to provide services to users interested in jointly exploiting different RISIS datasets and various Linked Open Data resources with the goal to explore, retrieve and visualize results of data analysis for their research purposes.
</details>
