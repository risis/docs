FROM lscr.io/linuxserver/grav

# disable ipv6 on nginx
RUN sed -i '/listen \[::\]:.\+;/d' /defaults/nginx/site-confs/default.conf.sample

COPY . /config/www/user/
